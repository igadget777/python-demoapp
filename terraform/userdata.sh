#!/bin/bash

sudo apt update 
sudo snap install docker
sudo addgroup --system docker
sudo adduser ubuntu docker
newgrp docker
sudo snap disable docker
sudo snap enable docker
cat /etc/group | grep docker >> /home/ubuntu/docker.txt
echo "Docker installed and ubuntu user added to docker group" >> /home/ubuntu/docker.txt